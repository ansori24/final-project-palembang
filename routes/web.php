<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/question');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('auth')->group(function() {
    Route::resource('/question', 'QuestionController');
    Route::get('/profile', 'ProfileController@index')->name('profile.index');
    Route::get('/profile/comment', 'ProfileCommentController@index')->name('profile.comment.index');
    Route::post('comment/{comment}/comment', 'CommentCommentController@store')->name('comment.comment.store');
    Route::post('question/{question}/comment', 'QuestionCommentController@store')->name('question.comment.store');
    Route::post('comment/{comment}/vote', 'CommentVoteController@store')->name('comment.vote.store');
    Route::post('question/{question}/vote', 'QuestionVoteController@store')->name('question.vote.store');
});