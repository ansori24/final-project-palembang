<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Question;
use App\Reputation;
use Faker\Generator as Faker;

$factory->define(Reputation::class, function (Faker $faker) {
    return [
        'user_id' => factory(User::class),
        'reputable_type' => 'App\\Question',
        'reputable_id' => factory(Question::class),
        'poin' => $faker->randomElement([10, -1]),
    ];
});
