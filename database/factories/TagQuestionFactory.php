<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Tag;
use App\Question;
use App\TagQuestion;
use Faker\Generator as Faker;

$factory->define(TagQuestion::class, function (Faker $faker) {
    return [
        'tag_id'      => factory(Tag::class),
        'question_id' => factory(Question::class),
    ];
});
