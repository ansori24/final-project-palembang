<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Comment;
use App\Question;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'user_id'          => factory(User::class),
        'commentable_type' => 'App\\Question',
        'commentable_id'   => factory(Question::class),
        'body'             => $faker->paragraph(rand(1, 5)),
    ];
});
