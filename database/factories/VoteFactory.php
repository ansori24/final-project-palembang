<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Vote;
use Faker\Generator as Faker;

$factory->define(Vote::class, function (Faker $faker) {
    return [
        'user_id'      => factory(User::class),
        'votable_type' => 'App\\Comment',
        'votable_id'   => factory(Comment::class),
        'poin'         => 10,
    ];
});
