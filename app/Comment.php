<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'commentable',
        'body',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
    ];

    /**
     * Comments belongs to User.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Comments has many Comment (using polymorphism).
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
    
    /**
     * Comments has many Votes (using polymorphism).
     */
    public function votes()
    {
        return $this->morphMany(Vote::class, 'votable');
    }
    
    /**
     * Comments has many UpVotes (using polymorphism).
     */
    public function upVotes()
    {
        return $this->morphMany(Vote::class, 'votable')->where('poin', 1);
    }

    /**
     * Comments has many downVotes (using polymorphism).
     */
    public function downVotes()
    {
        return $this->morphMany(Vote::class, 'votable')->where('poin', -1);
    }

    /**
     * Comments has many reputation (using polymorphism).
     */
    public function reputation()
    {
        return $this->morphMany(Reputation::class, 'reputable');
    }

    /**
     * Comments has total reputation.
     * 
     * @return integer
     */
    public function totalReputation()
    {
        return $this->morphMany(Reputation::class, 'reputable')->sum('poin');
    }
}
