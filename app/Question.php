<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'body',
        'correct_comment_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'correct_comment_id' => 'integer',
    ];

    /**
     * Questions belong to User.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Question has correct Comment.
     */
    public function correctComment()
    {
        return $this->belongsTo(Comment::class);
    }

    /**
     * Question has many Tags
     */
    public function tag() {
        return $this->belongsToMany(Tag::class, 'tag_questions', 'question_id', 'tag_id')->withTimestamps();
    }

    /**
     * Comments has many Comment (using polymorphism).
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
    
    /**
     * Comments has many Votes (using polymorphism).
     */
    public function votes()
    {
        return $this->morphMany(Vote::class, 'votable');
    }
    
    /**
     * Comments has many UpVotes (using polymorphism).
     */
    public function upVotes()
    {
        return $this->morphMany(Vote::class, 'votable')->where('poin', 1);
    }

    /**
     * Comments has many downVotes (using polymorphism).
     */
    public function downVotes()
    {
        return $this->morphMany(Vote::class, 'votable')->where('poin', -1);
    }

    /**
     * Comments has many reputation (using polymorphism).
     */
    public function reputation()
    {
        return $this->morphMany(Reputation::class, 'reputable');
    }

    /**
     * Comments has total reputation.
     * 
     * @return integer
     */
    public function totalReputation()
    {
        return $this->morphMany(Reputation::class, 'reputable')->sum('poin');
    }
}
