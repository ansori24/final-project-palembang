<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * User has many Questions.
     */
    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    /**
     * User has many Commments.
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
    * User has many Reputations.
    */
    public function reputations()
    {
        return $this->hasMany(Reputation::class);
    }

    /**
     * User has many Votes.
     */
    public function votes()
    {
        return $this->hasMany(Vote::class);
    }

    /**
     * User has many UpVotes.
     */
    public function upVotes()
    {
        return $this->hasMany(Vote::class)->where('poin', 1);
    }

    /**
     * User has many DownVotes.
     */
    public function downVotes()
    {
        return $this->hasMany(Vote::class)->where('poin', -1);
    }
}
