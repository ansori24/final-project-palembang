<?php

namespace App\Http\Controllers;

use Str;
use Auth;
use App\Tag;
use App\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $questions = Question::with('user')
            ->withCount('comments')
            ->latest()
            ->simplePaginate();

        return view('question.index', [
            'questions' => $questions
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('question.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $user_id = Auth::user()->id;

        $tags = explode(",", $request->tag);
        foreach($tags as $tag) {
            $tag_created    = Tag::firstOrCreate([
                                    "name" => trim($tag),
                                    "slug" => Str::slug($tag)
                              ]);
            $tag_id[]       = $tag_created->id;
        }

        $question = new Question;
        $question->user_id  = $user_id;
        $question->title    = $request->title;
        $question->body     = $request->body;
        $question->save();

        $question->tag()->sync($tag_id);

        return redirect()->route('question.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        $question->load('comments.comments');
        
        return view('question.show', [
            'question' => $question
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        $question->load('tag');

        foreach($question->tag as $tag){
            $tags[] = $tag->name;
        }

        return view('question.edit', [
            'question' => $question,
            'tag'     => implode(',', $tags)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {
        $tags               = explode(",", $request->tag);
        
        foreach($tags as $tag) {
            $tag_created    = Tag::firstOrCreate([
                                "name" => trim($tag),
                                "slug" => Str::slug($tag)
                              ]);
            $tag_id[] = $tag_created->id;             
        }

        $question->title    = $request->title;
        $question->body     = $request->body;
        $question->save();
        
        $question->tag()->sync($tag_id);

        return redirect()->route('question.show', $question);
    }
}
