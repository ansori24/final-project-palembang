<?php

namespace App\Http\Controllers;

class ProfileCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('profile.comments');
    }
}
