<?php

namespace App\Http\Controllers;

use App\Vote;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentVoteController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Comment $comment, Request $request)
    {
        // Find if the user has already voting.
        $existsVote = Vote::query()
            ->whereUserId(Auth::id())
            ->whereVotableId($comment->id)
            ->whereVotableType('App\\Comment')
            ->exists();

        // if vote exists then redirect back.
        if ($existsVote) {
            return redirect()->back();
        }
        
        // else, create Vote for Comment.
        $comment->votes()->create([
            'user_id' => Auth::id(),
            'poin' => $request->vote == 'upvote' ? 1 : -1
        ]);

        // and, create Reputation for Comment.
        $comment->reputation()->create([
            'user_id' => Auth::id(),
            'poin' => $request->vote == 'upvote' ? 10 : -1
        ]);

        return redirect()
            ->back();
    }
}
