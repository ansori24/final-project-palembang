<?php

namespace App\Http\Controllers;

use App\Vote;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuestionVoteController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Question $question, Request $request)
    {
        // Find if the user has already voting.
        $existsVotes = Vote::query()
            ->whereUserId(Auth::id())
            ->whereVotableId($question->id)
            ->whereVotableType('App\\Question')
            ->exists();

        // if vote exists then redirect back.
        if ($existsVotes) {
            return redirect()->back();
        }
        
        // else, create Vote for Question.
        $question->votes()->create([
            'user_id' => Auth::id(),
            'poin' => $request->vote == 'upvote' ? 1 : -1
        ]);

        // and, create Reputation for Question.
        $question->reputation()->create([
            'user_id' => Auth::id(),
            'poin' => $request->vote == 'upvote' ? 10 : -1
        ]);

        return redirect()
            ->back();
    }
}
