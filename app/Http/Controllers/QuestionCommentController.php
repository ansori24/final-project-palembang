<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuestionCommentController extends Controller
{    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Question $question, Request $request)
    {
        // Validation
        $request->validate([
            'body' => 'required',
        ]);

        // Create new Comment for Question.
        $question->comments()->create([
            'user_id' => Auth::id(),
            'body' => $request->body
        ]);

        return redirect()
            ->route('question.show', $question)
            ->with('success', 'Comment added');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        $question->load('tag');

        foreach($question->tag as $tag){
            $tags[] = $tag->name;
        }

        return view('question.edit', [
            'question' => $question,
            'tag'     => implode(',', $tags)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {
        $question->title    = $request->title;
        $question->body     = $request->body;
        $question->save();

        $tags       = explode(",", $request->tag);
        $tags_model = [];

        foreach($tags as $tag) {
            $tag_created    = Tag::updateOrCreate([
                                 "slug" => Str::slug($tag)
                              ],
                              [
                                 "name" => trim($tag)
                              ]);
            $tags_model[]   = $tag_created;
        }

        foreach($tags_model as $tag){
            $question->tag()->attach($tag->id);
        }

        return redirect()->route('question.index');
    }
}
