@extends('profile.index')

@section('profile')
    <ul>
        @forelse (Auth::user()->questions as $question)           
            <li>
                <a href="{{ route('question.show', $question) }}" class="block transition duration-150 ease-in-out hover:bg-gray-50 focus:outline-none focus:bg-gray-50">
                    <div class="flex items-center px-4 py-4 sm:px-6">
                        <div class="flex items-center flex-1 min-w-0">
                            <div class="flex-shrink-0">                                        
                                <img class="w-12 h-12 rounded-full" src="https://i.pravatar.cc/150?img={{ $question->user_id }}" alt="">
                            </div>
                            <div class="flex-1 min-w-0 px-4 md:grid md:grid-cols-2 md:gap-4">
                                <div>
                                    <div class="text-lg font-medium leading-5 truncate">{{ $question->title }}</div>
                                    <div class="flex items-center mt-2 text-sm leading-5 text-gray-500">
                                        by : <span class="ml-1 text-sm text-indigo-600 "> {{ $question->user->name }}</span>
                                    </div>
                                </div>
                                <div class="hidden md:block">
                                    <div>
                                        <div class="text-sm leading-5 text-gray-900">
                                            Created at
                                            <time datetime="2020-01-07">{{ $question->created_at->diffForHumans() }}</time>
                                        </div>
                                        <div class="flex items-center">
                                            @if ($question->correct_comment_id)
                                                <div class="flex items-center mt-2 text-sm leading-5 text-gray-500">
                                                    <svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-green-400" fill="currentColor" viewBox="0 0 20 20">
                                                        <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                                                    </svg>
                                                    Completed 
                                                </div>
                                                <span class="mx-2 text-4xl leading-3 text-gray-400 text-bold">&middot;</span>
                                            @endif
                                            <div class="flex items-center mt-2 text-sm leading-5 text-gray-500">
                                                <svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-green-400" fill="currentColor" viewBox="0 0 24 24">
                                                    <path class="heroicon-ui" d="M2 15V5c0-1.1.9-2 2-2h16a2 2 0 0 1 2 2v15a1 1 0 0 1-1.7.7L16.58 17H4a2 2 0 0 1-2-2zM20 5H4v10h13a1 1 0 0 1 .7.3l2.3 2.29V5z" />
                                                </svg>
                                                {{ $question->comments->count() }} {{ Str::plural('comment', $question->comments_count) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <svg class="w-5 h-5 text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                            </svg>
                        </div>
                    </div>
                </a>
            </li>
        @empty
            <li class="p-4 text-sm italic text-center text-gray-700" >
                Sorry, You dont have any questions yet.
            </li>
        @endforelse
    </ul>
@endsection