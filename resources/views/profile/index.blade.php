@extends('layouts.master')

@section('header')
    <header class="py-10">
        <div class="max-w-4xl px-4 mx-auto sm:px-6 lg:px-8">
            <div class="lg:flex lg:items-center lg:justify-between">
                <div class="flex-1 min-w-0">
                    <h2 class="text-2xl font-bold leading-7 text-white capitalize sm:text-3xl sm:leading-9 sm:truncate">
                        {{ Auth::user()->name }}
                    </h2>
                    <div class="flex flex-col mt-1 sm:mt-0 sm:flex-row sm:flex-wrap">
                        <div class="flex items-center mt-2 text-sm leading-5 text-gray-300 sm:mr-6">
                            <svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-500" fill="currentColor" viewBox="0 0 24 24">
                                <path class="heroicon-ui" d="M12.76 3.76a6 6 0 0 1 8.48 8.48l-8.53 8.54a1 1 0 0 1-1.42 0l-8.53-8.54a6 6 0 0 1 8.48-8.48l.76.75.76-.75zm7.07 7.07a4 4 0 1 0-5.66-5.66l-1.46 1.47a1 1 0 0 1-1.42 0L9.83 5.17a4 4 0 1 0-5.66 5.66L12 18.66l7.83-7.83z" />
                            </svg>
                            {{ Auth::user()->reputations()->sum('poin') }} poin Reputation
                        </div>
                        <div class="flex items-center mt-2 text-sm leading-5 text-gray-300 sm:mr-6">
                            <svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-500 mb-1" fill="currentColor" viewBox="0 0 24 24">
                                <path class="heroicon-ui" d="M17.62 10H20a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H8.5c-1.2 0-2.3-.72-2.74-1.79l-3.5-7-.03-.06A3 3 0 0 1 5 9h5V4c0-1.1.9-2 2-2h1.62l4 8zM16 11.24L12.38 4H12v7H5a1 1 0 0 0-.93 1.36l3.5 7.02a1 1 0 0 0 .93.62H16v-8.76zm2 .76v8h2v-8h-2z" />
                            </svg>
                            {{ Auth::user()->upVotes->count() }} 
                        </div>
                        <div class="flex items-center mt-2 text-sm leading-5 text-gray-300 sm:mr-6">
                            <svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-500 mt-1" fill="currentColor" viewBox="0 0 24 24">
                                <path class="heroicon-ui" d="M6.38 14H4a2 2 0 0 1-2-2V4c0-1.1.9-2 2-2h11.5c1.2 0 2.3.72 2.74 1.79l3.5 7 .03.06A3 3 0 0 1 19 15h-5v5a2 2 0 0 1-2 2h-1.62l-4-8zM8 12.76L11.62 20H12v-7h7c.13 0 .25-.02.38-.08a1 1 0 0 0 .55-1.28l-3.5-7.02A1 1 0 0 0 15.5 4H8v8.76zM6 12V4H4v8h2z" />
                            </svg>
                            {{ Auth::user()->downVotes->count() }}
                        </div>
                        <div class="flex items-center mt-2 text-sm leading-5 text-gray-300">
                            <svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-500" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z" clip-rule="evenodd" />
                            </svg>
                            Join on  {{ Auth::user()->created_at->format('d M, Y') }}
                        </div>
                    </div>
                </div>
                <div class="flex mt-5 lg:mt-0 lg:ml-4">
                    <span x-data="{ open: false }" class="relative ml-3 rounded-md shadow-sm sm:hidden">
                        <button @click="open = !open" type="button" class="inline-flex items-center px-4 py-2 text-sm font-medium leading-5 text-white transition duration-150 ease-in-out bg-gray-700 border border-transparent rounded-md hover:bg-gray-600 focus:outline-none focus:shadow-outline-gray focus:border-gray-800">
                            More
                            <svg class="w-5 h-5 ml-2 -mr-1 text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                            </svg>
                        </button>

                        <div x-show="open" x-transition:enter="transition ease-out duration-200" x-transition:enter-start="transform opacity-0 scale-90" x-transition:enter-end="transform opacity-100 scale-100" x-transition:leave="transition ease-in duration-75" x-transition:leave-start="transform opacity-100 scale-100" x-transition:leave-end="transform opacity-0 scale-90"
                            class="absolute left-0 w-48 mt-2 -ml-1 origin-top-left rounded-md shadow-lg">
                            <div class="py-1 bg-white rounded-md shadow-xs">
                                <a href="#" class="block px-4 py-2 text-sm leading-5 text-gray-700 transition duration-150 ease-in-out hover:bg-gray-100 focus:outline-none focus:bg-gray-100">Edit</a>
                                <a href="#" class="block px-4 py-2 text-sm leading-5 text-gray-700 transition duration-150 ease-in-out hover:bg-gray-100 focus:outline-none focus:bg-gray-100">View</a>
                            </div>
                        </div>
                    </span>
                </div>
            </div>
        </div>
    </header>
@endsection

@section('content')
    <div class="max-w-4xl px-4 pb-12 mx-auto sm:px-6 lg:px-8">
        <div class="overflow-hidden bg-white rounded-md shadow" x-data="{ open: false }">
            <div class="px-4 pt-5 bg-white border-b border-gray-200 sm:px-6">
                <h3 class="text-lg font-medium leading-6 text-gray-900">
                    Activities
                </h3>
                <div class="pt-3 pb-4 sm:p-0">
                    <div>
                        <nav class="flex -mb-px">
                            <a href="{{ route('profile.index') }}" class="px-1 py-4 text-sm font-medium leading-5 whitespace-no-wrap transition duration-150 ease-in-out focus:outline-none {{ Request::route()->getName() == 'profile.index' ? 'border-b-2 text-indigo-600 border-indigo-500 focus:text-indigo-800 focus:border-indigo-700' : 'hover:border-gray-300 hover:text-gray-700 hover:border-gray-300 focus:text-gray-700 focus:border-gray-300' }}">
                                All Question
                            </a>
                            <a href="{{ route('profile.comment.index') }}" class="px-1 py-4 ml-8 text-sm font-medium leading-5 whitespace-no-wrap transition duration-150 ease-in-out focus:outline-none {{ Request::route()->getName() == 'profile.comment.index' ? 'border-b-2 text-indigo-600 border-indigo-500 focus:text-indigo-800 focus:border-indigo-700' : 'hover:border-gray-300 hover:text-gray-700 hover:border-gray-300 focus:text-gray-700 focus:border-gray-300' }}">
                                All Comments
                            </a>
                        </nav>
                    </div>
                </div>
            </div>
            @yield('profile')
        </div>
    </div>
@endsection