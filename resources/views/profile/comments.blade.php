@extends('profile.index')

@section('profile')
    <div>
        @foreach (Auth::user()->comments as $comment)
            <div class="my-2 overflow-hidden bg-white rounded-md shadow">
                <div class="flex p-4 mb-2 bg-white border-gray-200 sm:p-6">
                    <img class="w-12 h-12 rounded-full" src="https://i.pravatar.cc/150?img={{ $comment->user_id }}" alt="">
                    <div class="w-full ml-6" x-data="{ showForm : false}">
                        <div class="flex items-center justify-between">
                            <div class="flex items-center justify-between">
                                <a href="#" class="text-blue-500">{{ $comment->user->name }}</a>
                            </div>
                            <div class="flex items-center justify-center w-8 h-8 mt-2 mr-8 text-sm font-semibold text-green-700 bg-green-100 rounded-full">{{ $comment->totalReputation() }}</div>    
                        </div>
                        <p class="mt-1 text-sm">{!! $comment->body !!}</p>
                        <div class="flex items-center justify-between mt-1 mr-6 text-sm text-gray-400">
                            <div class="flex items-center">
                                <button class=" hover:text-blue-500 hover:underline"  x-on:click="showForm = !showForm">Reply</button>
                                <span class="mx-2 text-4xl leading-3 text-bold">&middot;</span>
                                @include('components.upvotes', ['model' => $comment])
                                <span class="mx-2 text-4xl leading-3 text-bold">&middot;</span>
                                @include('components.downvotes', ['model' => $comment])
                            </div>
                            <div>
                                {{ $comment->created_at->diffForHumans() }}
                            </div>
                        </div>
                        @include('components.comment-form', ['model' => $comment])

                        <div>
                            @foreach ($comment->comments as $subComment)
                                <div class="relative flex mt-4">
                                    <img class="w-12 h-12 rounded-full" src="https://i.pravatar.cc/150?img={{ $subComment->user_id }}" alt="">
                                    <div class="w-full ml-3 mr-6">
                                        <div class="flex items-center justify-between">
                                            <a href="#" class="text-blue-500">{{ $subComment->user->name }}</a>
                                            <div class="flex items-center justify-center w-8 h-8 mt-2 mr-2 text-sm font-semibold text-green-700 bg-green-100 rounded-full">{{ $subComment->totalReputation() }}</div>
                                        </div>
                                        <p class="mt-1 text-sm">{!! $subComment->body !!}</p>
                                        <div class="flex items-center justify-between mt-1 text-sm text-gray-400">
                                            <div class="flex items-center">
                                                @include('components.upvotes', ['model' => $subComment])
                                                <span class="mx-2 text-4xl leading-3 text-bold">&middot;</span>
                                                @include('components.downvotes', ['model' => $subComment])
                                            </div>
                                            <div>
                                                {{ $subComment->created_at->diffForHumans() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection