<form action="{{ route( strtolower(class_basename($model)) . '.vote.store', $model) }}" method="POST">
    @csrf
    <input type="hidden" name="vote" value="upvote">
    <button class="flex items-center hover:text-blue-500 hover:underline">
        <svg class="w-5 h-5 mb-1 fill-current" viewBox="0 0 24 24">
            <path class="heroicon-ui" d="M17.62 10H20a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H8.5c-1.2 0-2.3-.72-2.74-1.79l-3.5-7-.03-.06A3 3 0 0 1 5 9h5V4c0-1.1.9-2 2-2h1.62l4 8zM16 11.24L12.38 4H12v7H5a1 1 0 0 0-.93 1.36l3.5 7.02a1 1 0 0 0 .93.62H16v-8.76zm2 .76v8h2v-8h-2z" />
        </svg>
        <span> {{ $model->upVotes->count() }}</span>
    </button>
</form>