<form x-show.transition.origin.top.left="showForm" action="{{ route(strtolower(class_basename($model)) . '.comment.store', $model) }}" method="POST">
    @csrf
    <h5 class="mt-4 mb-2 text-2xl font-semibold text-gray-800">Leave a comment</h5>
    <textarea name="body" class="body" cols="30" rows="10"></textarea>
    <div class="flex flex-wrap items-center mt-4">
        <button type="submit" class="px-4 py-2 font-bold text-gray-100 bg-blue-500 rounded hover:bg-blue-700 focus:outline-none focus:shadow-outline">
            Submit comment
        </button>
    </div>     
</form>