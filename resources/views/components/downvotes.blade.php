<form action="{{ route( strtolower(class_basename($model)) . '.vote.store', $model) }}" method="POST">
    @csrf
    <input type="hidden" name="vote" value="downvote">
    <button class="flex items-center hover:text-blue-500 hover:underline">
        <svg class="w-5 h-5 mt-1 fill-current" viewBox="0 0 24 24">
            <path class="heroicon-ui" d="M6.38 14H4a2 2 0 0 1-2-2V4c0-1.1.9-2 2-2h11.5c1.2 0 2.3.72 2.74 1.79l3.5 7 .03.06A3 3 0 0 1 19 15h-5v5a2 2 0 0 1-2 2h-1.62l-4-8zM8 12.76L11.62 20H12v-7h7c.13 0 .25-.02.38-.08a1 1 0 0 0 .55-1.28l-3.5-7.02A1 1 0 0 0 15.5 4H8v8.76zM6 12V4H4v8h2z" />
        </svg>
        <span> {{ $model->downVotes->count() }}</span>
    </button>
</form>