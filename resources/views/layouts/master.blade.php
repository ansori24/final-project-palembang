<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    
    <!-- Fonts -->
    <link rel="stylesheet" href="https://rsms.me/inter/inter.css">
    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>

<body class="font-sans antialiased text-gray-900 bg-gray-100">
    <div>
        <div class="pb-32 bg-gray-800">
            @include('layouts.navbar')
            @yield('header')
        </div>
        <main class="-mt-32">
            @yield('content')
        </main>
    </div>

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@2.0.1/dist/alpine.js" defer></script>
    <script src="{{ mix('js/app.js') }}"></script>
    @stack('scripts')
</body>

</html>
