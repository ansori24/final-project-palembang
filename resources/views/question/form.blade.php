<div class="flex flex-wrap mb-3">
    <label for="title" class="block mb-2 text-sm font-bold text-gray-700">
        Question Title
    </label>

    <input id="title" type="text" class="form-input w-full @error('title') border-red-500 @enderror" name="title" value="{{ old('title', $question->title ?? '') }}" autocomplete="title" autofocus>

    @error('title')
        <p class="mt-4 text-xs italic text-red-500">
            {{ $message }}
        </p>
    @enderror
</div>

<div class="flex flex-wrap mb-3">
    <label for="body" class="block mb-2 text-sm font-bold text-gray-700">
        Description
    </label>

    <textarea id="body" class="form-input w-full @error('body') border-red-500 @enderror" name="body" rows="10">{{ old('body', $question->body ?? '') }}</textarea>

    @error('password')
        <p class="mt-4 text-xs italic text-red-500">
            {{ $message }}
        </p>
    @enderror
</div>

<div class="flex flex-wrap mb-3">
    <label for="title" class="block mb-2 text-sm font-bold text-gray-700">
        Tag
    </label>
    
    <input id="tag" type="text" class="form-input w-full @error('tag') border-red-500 @enderror" name="tag" autocomplete="tag" value="{{ old('tag', $tag ?? '')  }}" autofocus>

    @error('tag')
        <p class="mt-4 text-xs italic text-red-500">
            {{ $message }}
        </p>
    @enderror
</div>

@push('scripts')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>tinymce.init({selector:'#body'});</script>
@endpush