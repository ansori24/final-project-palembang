@extends('layouts.master')

@section('header')
    <header class="py-10">
        <div class="max-w-4xl px-4 mx-auto sm:px-6 lg:px-8">
            <div class="lg:flex lg:items-center lg:justify-between">
                <div class="flex-1 min-w-0">
                    <h2 class="text-2xl font-bold leading-7 text-white sm:text-3xl sm:leading-9 sm:truncate">
                        {{ $question->title }}
                    </h2>
                </div>
                <a href="{{ route('question.edit', $question) }}" class="flex items-center px-4 py-1 text-sm text-white bg-blue-700 rounded-full">
                    <svg class="w-4 h-4 mr-1 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                        <path class="heroicon-ui" d="M6.3 12.3l10-10a1 1 0 0 1 1.4 0l4 4a1 1 0 0 1 0 1.4l-10 10a1 1 0 0 1-.7.3H7a1 1 0 0 1-1-1v-4a1 1 0 0 1 .3-.7zM8 16h2.59l9-9L17 4.41l-9 9V16zm10-2a1 1 0 0 1 2 0v6a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6c0-1.1.9-2 2-2h6a1 1 0 0 1 0 2H4v14h14v-6z" />
                    </svg>
                    Edit
                </a>
            </div>
        </div>
    </header>
@endsection

@section('content')
    <div class="max-w-4xl px-4 pb-12 mx-auto sm:px-6 lg:px-8">
        <div class="overflow-hidden bg-white rounded-md shadow">
            <div class="px-8 py-10 bg-white border-gray-200" x-data="{ showForm : false }">
                <div class="flex items-start justify-between">
                    <div class="flex items-start">
                        <div class="w-12 h-12 mr-2">
                            <img class="w-12 h-12 rounded-full" src="https://i.pravatar.cc/150?img={{ $question->user_id }}" alt="">
                        </div>
                        <a href="#" class="ml-2 text-xl font-semibold text-indigo-500">{{ $question->user->name }}</a>
                    </div>
                    <div class="flex items-center justify-center w-8 h-8 mr-4 text-sm font-semibold text-green-700 bg-green-100 rounded-full">{{ $question->totalReputation() }}</div>
                </div>
                <div class="ml-16 mr-4">{!! $question->body !!}</div>
                <div class="flex items-center justify-between mt-3 ml-16 text-sm text-gray-400">
                    <div class="flex items-center">
                        <button class="hover:text-blue-500 hover:underline" x-on:click="showForm = !showForm">Reply</button>
                        <span class="mx-2 text-4xl leading-3 text-bold">&middot;</span>
                        @include('components.upvotes', ['model' => $question])
                        <span class="mx-2 text-4xl leading-3 text-bold">&middot;</span>
                        @include('components.downvotes', ['model' => $question])
                    </div>
                    <div class="flex items-baseline mr-4">
                        <div class="flex items-baseline">
                            @foreach ($question->tag as $tag)
                                <span class="px-2 ml-1 text-xs text-blue-800 bg-blue-200 rounded-full">{{ $tag->name }}</span>
                            @endforeach
                        </div>
                        <span class="ml-4">{{ $question->created_at->diffForHumans() }}</span>
                    </div>
                </div>
                <div class="ml-16">
                    @include('components.comment-form', ['model' => $question])
                </div>
            </div>
        </div>
        <div>
            @foreach ($question->comments as $comment)
                <div class="my-2 overflow-hidden bg-white rounded-md shadow">
                    <div class="flex p-4 mb-2 bg-white border-gray-200 sm:p-6">
                        <img class="w-12 h-12 rounded-full" src="https://i.pravatar.cc/150?img={{ $comment->user_id }}" alt="">
                        <div class="w-full ml-6" x-data="{ showForm : false}">
                            <div class="flex items-center justify-between">
                                <div class="flex items-center justify-between">
                                    <a href="#" class="text-blue-500">{{ $comment->user->name }}</a>
                                </div>
                                <div class="flex items-center justify-center w-8 h-8 mt-2 mr-8 text-sm font-semibold text-green-700 bg-green-100 rounded-full">{{ $comment->totalReputation() }}</div>    
                            </div>
                            <p class="mt-1 text-sm">{!! $comment->body !!}</p>
                            <div class="flex items-center justify-between mt-1 mr-6 text-sm text-gray-400">
                                <div class="flex items-center">
                                    <button class=" hover:text-blue-500 hover:underline"  x-on:click="showForm = !showForm">Reply</button>
                                    <span class="mx-2 text-4xl leading-3 text-bold">&middot;</span>
                                    @include('components.upvotes', ['model' => $comment])
                                    <span class="mx-2 text-4xl leading-3 text-bold">&middot;</span>
                                    @include('components.downvotes', ['model' => $comment])
                                </div>
                                <div>
                                    {{ $comment->created_at->diffForHumans() }}
                                </div>
                            </div>
                            @include('components.comment-form', ['model' => $comment])

                            <div>
                                @foreach ($comment->comments as $subComment)
                                    <div class="relative flex mt-4">
                                        <img class="w-12 h-12 rounded-full" src="https://i.pravatar.cc/150?img={{ $subComment->user_id }}" alt="">
                                        <div class="w-full ml-3 mr-6">
                                            <div class="flex items-center justify-between">
                                                <a href="#" class="text-blue-500">{{ $subComment->user->name }}</a>
                                                <div class="flex items-center justify-center w-8 h-8 mt-2 mr-2 text-sm font-semibold text-green-700 bg-green-100 rounded-full">{{ $subComment->totalReputation() }}</div>
                                            </div>
                                            <p class="mt-1 text-sm">{!! $subComment->body !!}</p>
                                            <div class="flex items-center justify-between mt-1 text-sm text-gray-400">
                                                <div class="flex items-center">
                                                    @include('components.upvotes', ['model' => $subComment])
                                                    <span class="mx-2 text-4xl leading-3 text-bold">&middot;</span>
                                                    @include('components.downvotes', ['model' => $subComment])
                                                </div>
                                                <div>
                                                    {{ $subComment->created_at->diffForHumans() }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection


@push('scripts')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>tinymce.init({selector:'.body'});</script>
@endpush