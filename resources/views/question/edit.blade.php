@extends('layouts.master')

@section('header')
    <header class="py-10">
        <div class="max-w-4xl mx-auto px-4 sm:px-6 lg:px-8">
            <div class="lg:flex lg:items-center lg:justify-between">
                <div class="flex-1 min-w-0">
                    <h2 class="text-2xl font-bold leading-7 text-white sm:text-3xl sm:leading-9 sm:truncate">
                        Edit Questions
                    </h2>
                </div>
            </div>
        </div>
    </header>
@endsection

@section('content')
    <div class="max-w-4xl mx-auto px-4 pb-12 sm:px-6 lg:px-8">
        <div class="bg-white shadow overflow-hidden rounded-md">
            <div class="bg-white px-4 border-b border-gray-200 sm:px-6">
                <div class="pt-3 pb-4 sm:p-0">
                    <form class="w-full px-6 py-12" method="POST" action="{{ route('question.update', $question) }}">
                        @csrf
                        @method('PUT')
                        @include('question.form')
                        
                        <div class="flex flex-wrap items-center mt-4">
                            <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-gray-100 font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection