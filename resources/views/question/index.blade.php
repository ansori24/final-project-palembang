@extends('layouts.master')

@section('header')
    <header class="py-10">
        <div class="max-w-4xl px-4 mx-auto sm:px-6 lg:px-8">
            <div class="lg:flex lg:items-center lg:justify-between">
                <div class="flex-1 min-w-0">
                    <h2 class="text-2xl font-bold leading-7 text-white sm:text-3xl sm:leading-9 sm:truncate">
                        All Questions
                    </h2>
                </div>
                <div class="flex mt-5 lg:mt-0 lg:ml-4">
                    <span class="rounded-md shadow-sm sm:ml-3">
                        <a href="{{ route('question.create') }}" class="inline-flex items-center px-4 py-2 text-sm font-medium leading-5 text-white transition duration-150 ease-in-out bg-indigo-500 border border-transparent rounded-md hover:bg-indigo-400 focus:outline-none focus:shadow-outline-indigo focus:border-indigo-600">
                            <svg class="w-5 h-5 mr-2 -ml-1" fill="currentColor" viewBox="0 0 24 24">
                                <path class="heroicon-ui" d="M17 11a1 1 0 0 1 0 2h-4v4a1 1 0 0 1-2 0v-4H7a1 1 0 0 1 0-2h4V7a1 1 0 0 1 2 0v4h4z"/>
                            </svg>
                            Create new Questions
                        </a>
                    </span>
                </div>
            </div>
        </div>
    </header>
@endsection

@section('content')
    <div class="max-w-4xl px-4 pb-12 mx-auto sm:px-6 lg:px-8">
        <div class="overflow-hidden bg-white rounded-md shadow">
            <div class="px-4 pt-5 bg-white border-b border-gray-200 sm:px-6">
                <h3 class="text-lg font-medium leading-6 text-gray-900">
                    Candidates
                </h3>
                <div class="pt-3 pb-4 sm:p-0">
                    <div class="sm:hidden">
                        <select class="block w-full text-gray-900 transition duration-150 ease-in-out border-gray-300 form-select focus:outline-none focus:shadow-outline-blue focus:border-blue-300">
                            <option>Applied</option>
                            <option>Phone screening</option>
                            <option selected>Interview</option>
                            <option>Offer</option>
                            <option>Hired</option>
                        </select>
                    </div>
                    <div class="hidden sm:block">
                        <div>
                            <nav class="flex -mb-px">
                                <a href="#" class="px-1 py-4 text-sm font-medium leading-5 text-gray-500 whitespace-no-wrap transition duration-150 ease-in-out border-b-2 border-transparent hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300">
                                    Applied
                                </a>
                                <a href="#" class="px-1 py-4 ml-8 text-sm font-medium leading-5 text-gray-500 whitespace-no-wrap transition duration-150 ease-in-out border-b-2 border-transparent hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300">
                                    Phone screening
                                </a>
                                <a href="#" class="px-1 py-4 ml-8 text-sm font-medium leading-5 text-indigo-600 whitespace-no-wrap transition duration-150 ease-in-out border-b-2 border-indigo-500 focus:outline-none focus:text-indigo-800 focus:border-indigo-700">
                                    Interview
                                </a>
                                <a href="#" class="px-1 py-4 ml-8 text-sm font-medium leading-5 text-gray-500 whitespace-no-wrap transition duration-150 ease-in-out border-b-2 border-transparent hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300">
                                    Offer
                                </a>
                                <a href="#" class="px-1 py-4 ml-8 text-sm font-medium leading-5 text-gray-500 whitespace-no-wrap transition duration-150 ease-in-out border-b-2 border-transparent hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300">
                                    Hired
                                </a>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="px-4 pt-5 pb-3 sm:px-6">
                <div>
                    <label for="filter" class="sr-only">Search candidates</label>
                    <div class="flex rounded-md shadow-sm">
                        <div class="relative flex-grow focus-within:z-10">
                            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                <svg class="w-5 h-5 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                                    <path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd" />
                                </svg>
                            </div>
                            <input id="filter" class="block w-full py-2 pl-10 pr-3 text-gray-900 placeholder-gray-400 transition duration-150 ease-in-out border border-gray-300 rounded-none appearance-none rounded-l-md focus:outline-none focus:shadow-outline-blue focus:border-blue-300 sm:text-sm sm:leading-5" placeholder="John Doe" />
                        </div>
                        <button class="relative flex items-center px-3 py-2 -ml-px text-sm leading-5 text-gray-900 transition duration-150 ease-in-out border border-gray-300 rounded-r-md bg-gray-50 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10">
                            <svg class="w-5 h-5 text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M3 3a1 1 0 011-1h12a1 1 0 011 1v3a1 1 0 01-.293.707L12 11.414V15a1 1 0 01-.293.707l-2 2A1 1 0 018 17v-5.586L3.293 6.707A1 1 0 013 6V3z" clip-rule="evenodd" />
                            </svg>
                            <span class="ml-2">Filter</span>
                        </button>
                    </div>
                </div>
            </div>
            <ul>
                @foreach ($questions as $question)           
                    <li>
                        <a href="{{ route('question.show', $question) }}" class="block transition duration-150 ease-in-out hover:bg-gray-50 focus:outline-none focus:bg-gray-50">
                            <div class="flex items-center px-4 py-4 sm:px-6">
                                <div class="flex items-start flex-1 min-w-0">
                                    <div class="flex-shrink-0">                                        
                                        <img class="w-12 h-12 rounded-full" src="https://i.pravatar.cc/150?img={{ $question->user_id }}" alt="">
                                    </div>
                                    <div class="flex-1 min-w-0 px-4 md:grid md:grid-cols-2 md:gap-4">
                                        <div>
                                            <div class="font-medium font-semibold leading-5 truncate">{{ $question->title }}</div>
                                            <div class="flex items-center mt-2 text-sm leading-5 text-gray-500">
                                                by : <span class="ml-1 text-sm text-indigo-600 "> {{ $question->user->name }}</span>
                                            </div>
                                            <div class="flex my-2 -ml-1">
                                                @foreach ($question->tag as $tag)
                                                    <span class="px-2 ml-1 text-xs text-blue-800 bg-blue-200 rounded-full">{{ $tag->name }}</span>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="hidden md:block">
                                            <div>
                                                <div class="text-sm leading-5 text-gray-900">
                                                    Created at
                                                    <time datetime="2020-01-07">{{ $question->created_at->diffForHumans() }}</time>
                                                </div>
                                               <div class="flex items-center">
                                                    @if ($question->correct_comment_id)
                                                        <div class="flex items-center mt-2 text-sm leading-5 text-gray-500">
                                                            <svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-green-400" fill="currentColor" viewBox="0 0 20 20">
                                                                <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                                                            </svg>
                                                            Completed 
                                                        </div>
                                                        <span class="mx-2 text-4xl leading-3 text-gray-400 text-bold">&middot;</span>
                                                    @endif
                                                    <div class="flex items-center mt-2 text-sm leading-5 text-gray-500">
                                                        <svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-green-400" fill="currentColor" viewBox="0 0 24 24">
                                                            <path class="heroicon-ui" d="M2 15V5c0-1.1.9-2 2-2h16a2 2 0 0 1 2 2v15a1 1 0 0 1-1.7.7L16.58 17H4a2 2 0 0 1-2-2zM20 5H4v10h13a1 1 0 0 1 .7.3l2.3 2.29V5z" />
                                                        </svg>
                                                        {{ $question->comments_count }} {{ Str::plural('comment', $question->comments_count) }}
                                                    </div>
                                               </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <svg class="w-5 h-5 text-gray-400" fill="currentColor" viewBox="0 0 20 20">
                                        <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                                    </svg>
                                </div>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="flex items-center justify-between px-4 py-3 bg-white border-t border-gray-200 sm:px-6">
                <div class="hidden sm:block">
                    <p class="text-sm leading-5 text-gray-700">
                        Total 
                        <span class="font-medium">{{ $question->count() }}</span>
                        results
                    </p>
                </div>
                <div class="flex justify-between flex-1 sm:justify-end">
                    {{ $questions->links('vendor.pagination.simple-default') }}
                </div>
            </div>
        </div>
    </div>
@endsection