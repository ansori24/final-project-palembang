@extends('layouts.master')

@section('header')
    <header class="py-10">
        <div class="max-w-4xl px-4 mx-auto sm:px-6 lg:px-8">
            <div class="lg:flex lg:items-center lg:justify-between">
                <div class="flex-1 min-w-0">
                    <h2 class="text-2xl font-bold leading-7 text-white sm:text-3xl sm:leading-9 sm:truncate">
                        Create new Questions
                    </h2>
                </div>
            </div>
        </div>
    </header>
@endsection

@section('content')
    <div class="max-w-4xl px-4 pb-12 mx-auto sm:px-6 lg:px-8">
        <div class="overflow-hidden bg-white rounded-md shadow">
            <div class="px-4 bg-white border-b border-gray-200 sm:px-6">
                <div class="pt-3 pb-4 sm:p-0">
                    <form class="w-full px-6 py-12" method="POST" action="{{ route('question.store') }}">
                        @csrf
                        @include('question.form')
                        <div class="flex flex-wrap items-center mt-4">
                            <button type="submit" class="px-4 py-2 font-bold text-gray-100 bg-blue-500 rounded hover:bg-blue-700 focus:outline-none focus:shadow-outline">
                                Save
                            </button>
                        </div>                        
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection